#!/bin/bash
#######  SCRIPT FOR AUTOMATIC BACKUPS FOR NEXTCLOUD ###################
# Author: tangelov
# Version: 0.5 -- Adding support to upload to Google Drive
# Version: 0.2 -- Adding comments.
# Version: 0.1 -- Just work.
######################################################################

WEB_DIR="/var/www/nextcloud/"
BK_DIR="/media/disk/backup/tmp/"
DATA_DIR="/media/disk/datos/"
BK_DATE=$(date +%Y%m%d)
BBDD_PW=""
BK_USER=""
BK_BBDD=""
GPG_ACCOUNT=""

echo "`date -u` Starting backup of $HOSTNAME" | tee -a /var/log/site_backup.log
#0-Creating the TMP dir
mkdir $BK_DIR

#First we are going to create a backup of all the data  in /var/www/nextcloud/
rsync -Aax $WEB_DIR "${BK_DIR}nextcloud_dirbkp_${BK_DATE}"
echo "`date -u` Copy done. Ready for test content" | tee -a /var/log/site_backup.log
diff -rq --no-dereference $WEB_DIR "${BK_DIR}nextcloud_dirbkp_${BK_DATE}/"

#The second step is to create a dump of the BBDD of Nextcloud and mysql internal databases.
mysqldump --lock-tables -h 127.0.0.1 -u $BK_USER -p$BBDD_PW $BK_BBDD > "${BK_DIR}nextcloud-sqlbkp_${BK_DATE}.sql"
echo "`date -u` Nextcloud Database backup finished." | tee -a /var/log/site_backup.log

mysqldump --lock-tables --ignore-table=mysql.event -h 127.0.0.1 -u $BK_USER -p$BBDD_PW mysql > "${BK_DIR}mysql-sqlbkp_${BK_DATE}.sql"
echo "`date -u` Internal Database backup finished." | tee -a /var/log/site_backup.log

#The third step is to copy the users data
rsync -Aax $DATA_DIR "${BK_DIR}nextcloud_dirdata_${BK_DATE}"
#echo "`date -u` Copied users-data." | tee -a /var/log/site_backup.log

#The fourth step is to copy the important config files
rsync -Aax /etc/nginx/sites-available/nextcloud "${BK_DIR}/nextcloud_nginx_${BK_DATE}"
echo "`date -u` Copied nginx configuration." | tee -a /var/log/site_backup.log
cp -p /etc/php5/fpm/php.ini "${BK_DIR}nextcloud_php.ini_${BK_DATE}"
echo "`date -u` Copied PHP configuration." | tee -a /var/log/site_backup.log

#We create a tar.gz file with all the directories created
tar -czf "/media/disk/backup/nextcloud-backup_$BK_DATE.tar.gz" -C $BK_DIR .

#Encrypt the backup and delete the old one
gpg --encrypt -r $GPG_ACCOUNT "/media/disk/backup/nextcloud-backup_$BK_DATE.tar.gz"
echo "`date -u` Encrypted tar.gz." | tee -a /var/log/site_backup.log
echo "ID backup $BK_DATE: `md5sum "/media/disk/backup/nextcloud-backup_$BK_DATE.tar.gz.gpg" | awk '{print $1}'`" | tee -a /var/log/site
_backup.log

#We fix the permissions of the created file
chmod 600 "/media/disk/backup/nextcloud-backup_$BK_DATE.tar.gz.gpg"

#We delete the non-compressed files BETA
rm -rf $BK_DIR

#Deleting the old backups
find /media/disk/backup/next* -mtime +20 -exec rm {} \;

echo "`date -u` Finished. Good luck." | tee -a /var/log/site_backup.log
