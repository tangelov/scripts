#!/bin/bash

# Script to install Terraform from Hashicorp in your PC for Debian/Ubuntu or RPM based distributions

# Step 0: checking prerrequisites curl and unzip
if ! [[ -x "$(command -v curl)" ]] || ! [[ -x "$(command -v unzip)" ]]; then
    echo "Prerrequisites not installed".
    echo "Please do sudo apt-get install curl unzip in Debian based-os or sudo yum install curl unzip in Redhat based-os"
fi

# First we download Terraform
curl https://releases.hashicorp.com/terraform/0.11.11/terraform_0.11.11_linux_amd64.zip -o terraform.zip

# Later we unzip it and move it to a PATH location
unzip terraform.zip && rm terraform.zip && mv terraform ~/.local/bin/terraform
